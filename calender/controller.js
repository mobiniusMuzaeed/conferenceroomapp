var app = angular.module('CalendarModule', ['ngTagsInput']);
app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
app.controller('CalendarController', function($scope, CalendarService,$window,tags) {
  // body..
  date = new Date();
  $scope.hrs = date.getHours();
  $scope.mins = date.getMinutes();
  curr_mon = date.getMonth();
  $scope.dt = date.getDate();
  firstdate = new Date(date.getFullYear(), date.getMonth(), 1);
  lastdate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  fday = firstdate.getDate();
  lday = lastdate.getDate()
  monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  var addevent = document.getElementById("addevent_but");
  var alert_msg = document.getElementById("alert_body");
  var date_alert = document.getElementsByClassName("date");
  var cancel_alert = document.getElementById("show_events");
  var login_button = document.getElementById("login_button");
  $scope.User = "";
  // $scope.user = "";
  // $scope.password = "";
  $scope.alert_ms = "";
  $scope.month_name = monthNames[curr_mon];
  $scope.year = date.getFullYear();
  $scope.date = '';
  $scope.addevent_visibility = false;
  $scope.showevent_visibility = false;
  $scope.addevent_index;
  $scope.addevent_row;
  $scope.inp_event_name = "";
  // $scope.conf_rm = "Conference Room 1"
  $scope.cancel_event_date = ""
  $scope.cancel_event_index = ""
  $scope.results = [];
  $scope.rows = [{
    'value': [' ', ' ', ' ', ' ', ' ', ' ', ' ']
  }, {
    'value': [' ', ' ', ' ', ' ', ' ', ' ', ' ']
  }, {
    'value': [' ', ' ', ' ', ' ', ' ', ' ', ' ']
  }, {
    'value': [' ', ' ', ' ', ' ', ' ', ' ', ' ']
  }, {
    'value': [' ', ' ', ' ', ' ', ' ', ' ', ' ']
  }, {
    'value': [' ', ' ', ' ', ' ', ' ', ' ', ' ']
  }];
  $scope.events = [{
    'event_name': [],
    'event_from_time': [],
    'event_to_time': [],
    'conference_room': []
  }];

  $scope.length;
  $scope.rows = CalendarService.month($scope.rows, date);
  var date_alert_index = 0;

  /*checking for login status */

  if (sessionStorage.getItem("userId") == undefined || sessionStorage.getItem("userId") == '') {
    sessionStorage.setItem("userId", 0);
  } else {

    $scope.User = sessionStorage.getItem("userName")
  }
  $scope.showevent_icon = function(row, index, row_index) {

      if (row == ' ' || index == 0 || index == 6)
        return false;
      else {

        return true;
      }

    }
    // chexks for today  to differentiate from other days
  $scope.Check_today = function(row, index, row_index) {
    $scope.addevent_index = index;
    $scope.addevent_row = row;
    var result = CalendarService.checkevt($scope.rows, $scope.addevent_row, $scope.addevent_index);
    ind = date.getDay();
    $scope.date = row[index];
    date_alert_index = (row_index * 7) + index;
    var currentDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    var day = currentDate.getDate();
    if (result == "event") {
      if ($scope.dt == $scope.date) {
        return 'today';
      }
      if (day == $scope.date) {
        return 'today';
      }
    }
  }

  //this method calls the service method checkevt to ensure whether the date clicked is correct
  $scope.addevent_box = function(row, index, row_index) {
    angular.element("#event").removeClass("btn-error");
    $scope.alert_ms = '';
    $scope.event = '';
    $scope.selection = [];
    $scope.failureAlert_ms = '';
    var loginStatus = sessionStorage.getItem("user");
    $scope.customHide = true;
    // addevent.removeAttribute("data-target");
    // addevent.setAttribute("data-target", "#myModal")
    hours = date.getHours() % 12;
    // hours = date.getHours();
    minutes = date.getMinutes();
    if (date.getDate() == row[index]) {
      if (minutes < 15) {
        $scope.inp_event_time_from_hour = hours;
        $scope.inp_event_time_to_hour = hours;
        $scope.inp_event_time_from_minute = 30;
        $scope.inp_event_time_to_minute = 45;
      } else if (minutes < 30) {
        $scope.inp_event_time_to_hour = hours + 1;
        $scope.inp_event_time_from_hour = hours;
        $scope.inp_event_time_from_minute = 45;
        $scope.inp_event_time_to_minute = 00;
      } else if (minutes < 45) {
        $scope.inp_event_time_from_hour = hours + 1;
        $scope.inp_event_time_to_hour = hours + 1;
        $scope.inp_event_time_from_minute = 00;
        $scope.inp_event_time_to_minute = 15;
      } else {
        $scope.inp_event_time_from_hour = hours + 1;
        $scope.inp_event_time_to_hour = hours + 1;
        $scope.inp_event_time_from_minute = 15;
        $scope.inp_event_time_to_minute = 30;
      }
    } else {
      $scope.inp_event_time_from_hour = 9;
      $scope.inp_event_time_to_hour = 9;
      $scope.inp_event_time_from_minute = 0;
      $scope.inp_event_time_to_minute = 15;
    }
    width = document.body.clientWidth;
    height = document.body.clientHeight;
    var sec = document.getElementById("event")
    sec.style.width = width + "px"
    sec.style.height = (height + 5) + "px"
    $scope.addevent_index = index;
    $scope.addevent_row = row;
    var result = CalendarService.checkevt($scope.rows, $scope.addevent_row, $scope.addevent_index);
    ind = date.getDay();
    $scope.date = row[index];
    $scope.eventdate = $scope.date;
    //console.log?($scope.date)
    date_alert_index = (row_index * 7) + index;
    if (result == "event") {
      var loginStatus = sessionStorage.getItem("userId");

      if (($scope.dt == $scope.date || $scope.dt + 1 == $scope.date) || (ind == 5 && $scope.dt + 3 == $scope.date)) {

        if (loginStatus == 0) {
          angular.element('#myModal1').modal('show');
        } else {
          angular.element('#event').modal('show');
          $scope.addevent_visibility = true;
        }

      } else if ($scope.dt - 1 > $scope.date) {
        //do nothing
      } else {
        date_alert[date_alert_index].setAttribute("data-target", "#myModal")
        $scope.alert_ms = "You can book event only for today and tomorrow.";
        date_alert[date_alert_index].setAttribute("data-toggle", "modal");
        // 		$scope.alert_ms="You can book event only for today and tomorrow.";
        // 		date_alert[date_alert_index].setAttribute("data-toggle","modal");
      }

    } else if (result == "holiday") {
      $scope.alert_ms = "It's a holiday";
      date_alert[date_alert_index].setAttribute("data-toggle", "modal");
    } else {
      date_alert[date_alert_index].removeAttribute("data-toggle");
    }

  }

  //this method close the addevent pop up window when user clicks the close button
  // $scope.close_addevent = function() {
  //   $scope.addevent_visibility = false;
  //   $scope.inp_event_name = "";
  //
  // }
  // $scope.close_showevent = function() {
  //     $scope.showevent_visibility = false;
  //   }

    /*filters functionality starts*/
  $scope.aminities = ['Projector', 'Board', 'AC', 'Internet', 'Special Event'];
  // toggle selection for a given aminity by name
  $scope.toggleSelection = function(aminity) {
    console.log(aminity)
    var idx = $scope.selection.indexOf(aminity);
    if($scope.specialEvent==true){
      $scope.specialEvent=false;
    }
    // is currently selected
    if (idx > -1) {
      $scope.selection.splice(idx, 1);
    }

    // is newly selected
    else {
      if(aminity == 'Special Event'){
        $scope.specialEvent = true;
      }
      else{
        $scope.specialEvent = false;
      }
      $scope.selection.push(aminity);
    }
    console.log($scope.selection)
  };

  /*calculate Event Slots*/

  $scope.calculateEventSlot = function(slot, hr, min) {
    console.log(slot)
    $scope.inp_event_time_from_hour = hr;
    $scope.inp_event_time_from_minute = min;
    console.log(hr)
    console.log(min)

    var slotmins = parseInt(min) + parseInt(slot);
        if (slotmins >= 60) {
          if($scope.inp_event_time_from_hour==12){
            $scope.inp_event_time_to_hour=1;
          }else{
          $scope.inp_event_time_to_hour = parseInt(hr) + 1;
    }
          //$scope.inp_event_time_to_hour = parseInt(hr) + 1;

          if (slotmins == 60) {
            $scope.inp_event_time_to_minute = '00';
          } else {
            var toMinitues = slotmins - 60;
            if (toMinitues <= 9) {
              $scope.inp_event_time_to_minute = '0' + toMinitues;
              $scope.TimeValidation=""
              if($scope.inp_event_time_to_hour==20){
                $scope.inp_event_time_to_minute = '00';
                $scope.TimeValidation="max time you can book till 8 PM";
              }

            } else {
                $scope.TimeValidation=""
              $scope.inp_event_time_to_minute = toMinitues;
              if($scope.inp_event_time_to_hour==20){
                $scope.inp_event_time_to_minute = '00';
                $scope.TimeValidation="max time you can book till  8 PM"
              }

            }
          }
        } else {
          $scope.TimeValidation=""
          $scope.inp_event_time_to_hour = hr;
          $scope.inp_event_time_to_minute = slotmins;
          if($scope.inp_event_time_to_hour==20){
            $scope.inp_event_time_to_minute = '00';
            $scope.TimeValidation="max time you can book till  8 PM"
          }

        }
//     if (slotmins >= 60) {
//       if($scope.inp_event_time_from_hour==12){
//         $scope.inp_event_time_to_hour=1;
//       }else{
//       $scope.inp_event_time_to_hour = parseInt(hr) + 1;
// }
//       //$scope.inp_event_time_to_hour = parseInt(hr) + 1;
//
//       if (slotmins == 60) {
//         $scope.inp_event_time_to_minute = '00';
//       } else {
//         var toMinitues = slotmins - 60;
//         if (toMinitues <= 9) {
//           $scope.inp_event_time_to_minute = '0' + toMinitues;
//         } else {
//           $scope.inp_event_time_to_minute = toMinitues;
//         }
//       }
//     } else {
//       $scope.inp_event_time_to_hour = hr;
//       $scope.inp_event_time_to_minute = slotmins;
//
//     }

  }
// $scope.activetab=$('.nav-tabs .active').text()
$scope.activetab='Step1';
$scope.tabChange=function(){
  $scope.activetab='Step1';
}

  // add an event to db

  $scope.saveEvent = function(eventdate) {
    //        addevent.removeAttribute("data-target");
    //        addevent.setAttribute("data-target", "#myModal")
    if ($scope.event == "" || $scope.event == undefined) {
      $scope.alert_ms = "Enter Event Name.";
    } else {
      var request = {
        'eventTitle': $scope.event.eventTitle,
        'eventType': $scope.event.eventType,
        'eventSlot': $scope.event.eventSlot,
        'eventStart': $scope.inp_event_time_from_hour + ':' + $scope.inp_event_time_from_minute,
        'eventEnd': $scope.inp_event_time_to_hour + ':' + $scope.inp_event_time_to_minute,
        'amminities': $scope.selection,
        'participants': $scope.event.participants
      }
      console.log(request)


      var d = new Date();
      var day = date.getDate();
      console.log(eventdate)
      console.log(day)
      if (eventdate == day) {
        var d1 = new Date();
        d.setHours($scope.inp_event_time_from_hour);
        d.setMinutes($scope.inp_event_time_from_minute);
        var fromTime = d;
        d1.setHours($scope.inp_event_time_to_hour);
        d1.setMinutes($scope.inp_event_time_to_minute);
        var toTime = d1;
      } else {
        var tomorrow = new Date();
        var tomToTime = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        tomToTime.setDate(tomToTime.getDate() + 1);
        tomorrow.setHours($scope.inp_event_time_from_hour);
        tomorrow.setMinutes($scope.inp_event_time_from_minute);
        var fromTime = tomorrow;
        tomToTime.setHours($scope.inp_event_time_to_hour);
        tomToTime.setMinutes($scope.inp_event_time_to_minute);
        var toTime = tomToTime;
        console.log(fromTime);
        console.log(toTime);
      }
      var data = {
        'conferenceRoomNo': "conf1",
        'eventName': $scope.event.eventTitle,
        'from': fromTime,
        'to': toTime,
        'userId': sessionStorage.getItem("userId"),
        'userName': sessionStorage.getItem("userName"),
      }

      console.log(data);
      CalendarService.addEvent(data).then(function(response) {
        $scope.eventDetails = response.data;
        sessionStorage.setItem("userId", userDetails.userId);
        sessionStorage.setItem("userName", userDetails.userName);
        $scope.activetab='Step2';
        angular.element('#menuTabs a[href="#tab2"]').tab('show');

      }, function(response) {
        $scope.login_alert_ms = '';
        $scope.failureAlert_ms = response.data.error;
        angular.element("#event").addClass('btn-error');
      })
    }
  }
// edit event

  $scope.getTemplate = function (singleEvent) {
      if (singleEvent._id === $scope.AllEvents.selected._id) return 'edit';
      else return 'display';
  };
  $scope.editEvent = function (Event) {
      $scope.AllEvents.selected = angular.copy(Event);
  };
  $scope.updateEvent = function (index) {
      var currentDate = new Date();
     var eventDate = new Date($scope.AllEvents.selected.from);
     if(eventDate < currentDate){
         alert("Date error!!!");
     }
      $scope.AllEvents.item[index] = angular.copy($scope.AllEvents.selected);
      var data = {};
      data._id = $scope.AllEvents.item[index]._id;
      data.from = $scope.AllEvents.item[index].from;
      data.to = $scope.AllEvents.item[index].to;
      data.conferenceRoomNo = $scope.AllEvents.item[index].conferenceRoomNo;
      data.eventName = $scope.AllEvents.item[index].eventName;
      console.log(data);
      CalendarService.editEvent(data).then(function(response) {
      console.log(response);
      console.log(eventDate);
      CalendarService.getAllEvents(eventDate).then(function(response) {
      // $scope.AllEvents = response.data;
      console.log(response);
      $scope.AllEvents = {
          item: response.data,
          selected: {}
      };
      $scope.userid = sessionStorage.getItem('userId')
      }, function(response) {
        //$scope.login_alert_ms = '';
      })
        }, function(response) {
          //$scope.login_alert_ms = '';

        });
        $scope.reset();
     };

     $scope.reset = function () {
         $scope.AllEvents.selected = {};
     };

     $scope.click_addevent = function() {
       return date.getDate();
     }

  $scope.open_login = function() {
    login_button.setAttribute("data-toggle", "modal");
  }
  $scope.close_login = function() {
    // login_button.removeAttribute("data-toggle");
    angular.element('#myModal3').modal('hide');
    angular.element('body').removeClass('modal-open');
    angular.element('.modal-backdrop').remove();
  }
  $scope.close_modal1 = function() {

    angular.element('#myModal').modal('hide');
    angular.element('body').removeClass('modal-open');
    angular.element('.modal-backdrop').remove();
  }


  // hideAndShowEvent method show and hides the events of a particular day

  $scope.hideAndShowEvent = function(e, eventdate, tickPos) {
      //addes tick-day class to point the current day event
      $("#cal-slide-tick").prop("class", "tick-day" + tickPos)
      if ($scope.eventdate == undefined) {
        $scope.customHide = false;
        $scope.eventdate = eventdate;
      } else if ($scope.eventdate != eventdate) {
        $scope.customHide = false;
        $scope.eventdate = eventdate;
      } else if ($scope.eventdate == eventdate) {
        $scope.customHide = $scope.customHide === false ? true : false;
        $scope.eventdate = eventdate;
      }
      //addes hideAndShowDiv div to display total event of a particular day
      document.getElementById("hideAndShowDiv").style.display = 'block';
      var html = document.getElementById('hideAndShowDiv');
      angular.element(e.currentTarget).closest('.row').append(html);
      //checks for today and tommorow
      var evtdate = new Date();
      if (eventdate != evtdate.getDate()) {
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        evtdate = tomorrow;
      }
      //getAllEvents service gives the events array from api
      CalendarService.getAllEvents(evtdate).then(function(response) {
        // $scope.AllEvents = response.data;
        $scope.AllEvents = {
            item: response.data,
            selected: {}
        };
        $scope.currentDate = new Date();
        $scope.userid = sessionStorage.getItem('userId')
      }, function(response) {
        //$scope.login_alert_ms = '';
      })
    }
    //deletEvent method  deletes an event based on id
  $scope.deletEvent = function(id) {
    //deletEvent service  deletes an event based on id
    CalendarService.deletEvent(id).then(function(response) {
      // $scope.AllEvents = response.data;
      // $scope.userid = sessionStorage.getItem('userId')
      console.clear()
      console.log($scope.eventdate)
      var evtdate = new Date();
      if ($scope.eventdate != evtdate.getDate()) {
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        evtdate = tomorrow;
      }
      console.log(evtdate)
        //After delete, getAllEvents service is called to get updated events list
      CalendarService.getAllEvents(evtdate).then(function(response) {
        $scope.AllEvents = {
            item: response.data,
            selected: {}
        };
        $scope.userid = sessionStorage.getItem('userId')
          console.log(response)
      }, function(response) {
        //$scope.login_alert_ms = '';

      })
    }, function(response) {
      //$scope.login_alert_ms = '';

    })

  }
  $scope.cancel_event = function(id) {
    $scope.cancel_event_id = id;
  }
  $scope.confirmDeletEvent = function() {
    console.log($scope.cancel_event_id)
    $scope.deletEvent($scope.cancel_event_id)
  }
  $scope.sendEmail = function(eventdate,tags){
    $scope.allName = [];
      // console.log(JSON.stringify(tags));
      // console.log(eventdate);
    angular.forEach(tags, function(value, key) {
        $scope.allName.push(value.text);
      });
     console.log($scope.allName);

$scope.data =  {
  name:$scope.allName,
  date: eventdate
};

console.log($scope.data);
      CalendarService.sendMail($scope.data).then(function (response) {
              //console.log(response.data);
              alert(response);

          }, function (response) {

          });

      };
  $scope.logout = function() {
    $window.location.reload();
    sessionStorage.setItem("userId", 0);
    sessionStorage.setItem("userName", 0);

  }

        $scope.loadTags = function(query) {
          return tags.load();
        };


});
app.service('tags', function($q) {
  var tags = [
    { "text": "Abhijith" },
    { "text": "Aneesh" },
    { "text": "Amrita" },
    { "text": "Harsha" },
    { "text": "Kesvan" },
    { "text": "Mujahid" },
    { "text": "Sathish" },
    { "text": "Shashidhar" },
    { "text": "Sreevani" },
    { "text": "Vinit" },
  ];

  this.load = function() {
    var deferred = $q.defer();
    deferred.resolve(tags);
    return deferred.promise;
  };

});
