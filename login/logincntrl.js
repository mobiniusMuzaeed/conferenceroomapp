angular.module('loginModule', [])

.controller('logincntrl', function ($scope, CalendarService) {

    $scope.login = function () {
        var data = {};
        data.userName = $scope.user;
        data.password = $scope.password;
        CalendarService.signIn(data).then(function (response) {
            //console.log?(response.data);
            var userDetails = response.data;
            //console.log?(userDetails.id)
            $scope.login_alert_ms = "Sucessfully Logged In.";
            angular.element('#myModal1').modal('hide');
            $scope.User = userDetails.userName.substr(0, userDetails.userName.indexOf('@'));
            sessionStorage.setItem("userId", userDetails.id);
            sessionStorage.setItem("userName", $scope.User);
        }, function (response) {
            $scope.login_alert_ms = '';
            //console.log?(response.data.error);
            $scope.login_alert_ms = response.data.error;
        })


        /*if($scope.user==sessionStorage.getItem("user")&&$scope.password==sessionStorage.getItem("password")){
      $scope.login_alert_ms = "Sucessfully Logged In.";
      angular.element('#myModal1').modal('hide');
        $scope.User=$scope.user.substr(0, $scope.user.indexOf('@'));
      }
      else{
$scope.login_alert_ms = "Invalid Username or Password.";
      }*/
    }
    $scope.setRegister = function () {
        $scope.type = 'register';
    }
    $scope.setLogin = function () {
        $scope.type = 'login';
    }
    $scope.validate = function (email) {
        if (email != undefined) {
            var reg = /^[_a-zA-Z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
            var testvalidate = email.split('.');
            if (reg.test(email)) {
                var emailvalidate = testvalidate[testvalidate.length - 1];
                if (emailvalidate == 'com' || emailvalidate == 'in') {
                    $scope.sucess = ''
                    return true;
                } else {
                    $scope.sucess = ''
                    $scope.sucess = "please enter valid email user@mobinius.com";
                    return false;
                }
            } else {
                $scope.sucess = ''
                $scope.sucess = "please enter valid email user@mobinius.com";
                return false;
            }
        }
    }
})
