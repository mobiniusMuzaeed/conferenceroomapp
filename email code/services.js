app.service('CalendarService', function ($http) {
    this.signIn = function (data) {
        var promise = $http.post('/signin', data)
            .success(function (data, status) {
                //console.log?(data)
            })
            .error(function (data, status) {
                //console.log?(data)
            });

        return promise;
    }
    this.addEvent = function (data) {

        var returnEvent = $http.post('/addEvent', data)
            .success(function (data, status) {
                console.log(data)
            })
            .error(function (data, status) {
                //console.log?(data)
            });

        return returnEvent;
    }
    this.deletEvent = function (data) {
        //console.log?(data)
        var returnEvents = $http({
                url: '/deletEvent/' + data,
                method: "GET"
            }).success(function (data, status) {
                //console.log?(data)
            })
            .error(function (data, status) {
                //console.log?(data)
            });

        return returnEvents;
    }
    this.getAllEvents = function (data) {
            //console.log?('get all events')
            var returnAllEvents = $http({
                    url: '/allEvents',
                    method: "GET",
                    params: {
                        date: data
                    }
                }).success(function (data, status) {
                    //console.log?(data)
                })
                .error(function (data, status) {
                    //console.log?(data)
                });
            /*var returnAllEvents = $http.get('/allEvents', params: {
        data
    })
    .success(function (data, status) {
        //console.log?(data)
    })
    .error(function (data, status) {
        //console.log?(data)
    });*/

            return returnAllEvents;
        }

    //Send mail to participants

    this.sendMail = function (data) {
console.log("-------------------");
      console.log(data);
        var returnEvent = $http.post('/sendMail', data)
            .success(function (data, status) {
                console.log(data)
            })
            .error(function (data, status) {
                //console.log?(data)
            });

        return returnEvent;
    }
        //checkevt service is used to check whether we can add event on that particular selected date and it
        //will return the result accordings to the date
    this.checkevt = function (rows, row, index) {
        temp = "event";
        temp_1 = "not a event"
        if (row[index] == " ")
            return temp_1; // when user select the empty date box
        else if (index == 0 || index == 6)
            return "holiday"; //when user select the holiday
        else
            return temp;
    }

    this.add_event = function (rows, evt_dt, evt_name, from_time, to_time, conf_rm) {
        factory = rows;
        var temp;
        for (var i = 0; i < factory.length; i++) {
            for (j = 0; j < 7; j++) {
                if (factory[i].value[j] == evt_dt) {
                    factory[i].event_name[j].push(evt_name);
                    factory[i].event_from_time[j].push(from_time);
                    factory[i].event_to_time[j].push(to_time);
                    factory[i].conference_room[j].push(conf_rm);
                    break;
                }
            }
        }
        return factory;
    }

    this.showevents = function (rows, date) {
        factory = rows;
        events = [{
            'event_name': [],
            'event_from_time': [],
            'event_to_time': [],
            'conference_room': []
	  	}];
        for (var i = 0; i < factory.length; i++) {
            for (j = 0; j < 7; j++) {
                if (factory[i].value[j] == date) {
                    events[0].event_name.push(factory[i].event_name[j]);
                    events[0].event_from_time.push(factory[i].event_from_time[j]);
                    events[0].event_to_time.push(factory[i].event_to_time[j]);
                    events[0].conference_room.push(factory[i].conference_room[j]);
                    break;
                }
            }
        }
        return events;
    }

    this.cancel_evt = function (rows, evt_date, index, flag) {
        factory = rows;
        for (var i = 0; i < factory.length; i++) {
            for (j = 0; j < 7; j++) {
                if (factory[i].value[j] == evt_date) {
                    if (flag == 1) {
                        factory[i].event_name[j].splice(index, 1);
                        factory[i].event_from_time[j].splice(index, 1);
                        factory[i].event_to_time[j].splice(index, 1);
                        factory[i].conference_room[j].splice(index, 1);
                    }
                    break;
                }
            }
        }
        return factory;
    }


    this.save_events = function (data) {
        sessionStorage.setItem('events', JSON.stringify(data));
    }


    this.retrieve_events = function () {
            var obj = JSON.parse(sessionStorage.events);
            return obj;
        }
        //month service is used to fetch the calendar month and will give the month details..
    this.month = function (rows, date) {
        date = new Date(date);
        curr_mon = date.getMonth();
        firstdate = new Date(date.getFullYear(), date.getMonth(), 1);
        lastdate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        fday = firstdate.getDate();
        lday = lastdate.getDate();
        startdate_index = firstdate.getDay();
        enddate_index = lastdate.getDay();
        temp = fday;
        monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        month_name = monthNames[curr_mon];
        year = date.getFullYear();
        key = month_name + " " + year;
        factory = rows;
        for (var i = 0; i < factory.length; i++) {
            for (var j = 0; j < 7; j++) {
                factory[i].value[j] = " ";
                factory[i].event_name[j] = [];
                factory[i].event_from_time[j] = [];
                factory[i].event_to_time[j] = [];
            }
        }
        for (var i = 0; i < 6; i++) {
            for (var j = 0; j < 7; j++) {
                if (temp > lday) {
                    break;
                } else {
                    if (i == 0) {
                        if (j >= startdate_index) {
                            factory[i].value[j] = temp;
                            //factory[i].clickable[j]=true;
                            temp++;
                        } else {
                            factory[i].value[j] = " ";
                        }
                    } else {
                        factory[i].value[j] = temp;
                        //factory[i].clickable[j]=true;
                        temp++;
                    }
                }
            }
        }
        if (sessionStorage.events != null) {
            data = this.retrieve_events();
            if (data.hasOwnProperty(key)) {
                for (var z = 0; z < data[key].length; z++) {
                    for (var i = 0; i < factory.length; i++) {
                        for (var j = 0; j < 7; j++) {
                            if (factory[i].value[j] == data[key][z].date) {
                                factory[i].event_name[j].push(data[key][z].event_name);
                                factory[i].event_from_time[j].push(data[key][z].event_from_time);
                                factory[i].event_to_time[j].push(data[key][z].event_to_time);
                                factory[i].conference_room[j].push(data[key][z].conference_room);
                            }

                        }
                    }
                }
            }
        }

        return factory;
    }
});
